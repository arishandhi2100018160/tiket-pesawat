.model small
.code
org 100h
start:
   jmp mulai
nama   db 13,10,'Nama : $'
hp     db 13,10,'No HP : $'        
tampak_nama db 30,?,30 dup (?)
tampak_hp   db 13,?,13 dup (?)
tampak_kode db 13,?,13 dup (?)

a db 01
b db 02
c db 03
d db 04
e db 05
f db 06
g db 07
h db 08
i db 09
j dw 15

daftar db 13,10,'========================================================================'
       db 13,10,'      			BANDARA SOEKARNO HATTA		                 '
       db 13,10,'========================================================================'
       db 13,10,'		 Rute Penerbangan Jakarta-Bali				 '
       db 13,10,'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
       db 13,10,'NO|     Maskapai     |   Flight No |  Jam  | Tanggal   |  Harga Tiket  |'
       db 13,10,'01.| Citilink        |   QG-702    | 11.00 |03/03/2021 | Rp. 770.000   |'
       db 13,10,'02.| Sriwijaya Air   |   SJ-226    | 13.20 |03/03/2021 | Rp. 1.200.000 |'
       db 13,10,'03.| Lion Air        |   JT-601    | 12.40 |03/03/2021 | Rp. 550.000   |'
       db 13,10,'04.| Aviastar 	     |   MV-190    | 08.05 |03/03/2021 | Rp. 720.0000  |'
       db 13,10,'05.| Wings Air       |   IW-220    | 09.45 |03/03/2021 | Rp. 560.000   |' 
       db 13,10,'========================================================================'
       db 13,10,'     			PILIH MASKAPAI SESUAI KEBERANGKATAN           '
       db 13,10,'=======================================================================$'
      
error   db 13,10,'ANDA SALAH MEMASUKKAN KODE $'
pilih_ps db 13,10,'Silahkan masukkan Maskapai Sesuai jadwal yang diingankan $'
succes db 13,10,'SELAMAT $'

   mulai:
   mov ah,09h
   lea dx,nama
   int 21h
   mov ah,0ah
   lea dx,tampak_nama
   int 21h
   push dx
   
   mov ah,09h
   lea dx,hp
   int 21h
   mov ah,0ah
   lea dx,tampak_hp
   int 21h
   push dx
   
   mov ah,09h
   mov dx,offset daftar
   int 21h
   mov ah,09h
   mov ah,01h

   mov ah,09h 
   mov ah,01h
   jmp proses
   jne error_msg

error_msg:
   mov ah,09h
   mov dx,offset error
   int 21h
   int 20h

proses:
  mov ah,09h
  mov dx,offset pilih_ps
  int 21h
  
  mov ah,1
  int 21h
  mov bh,al
  mov ah,1
  int 21h
  mov bl,al
  cmp bh,'0'
  cmp bl,'1'
  je hasil1
  
  cmp bh,'0'
  cmp bl,'2'
  je hasil2
  
  cmp bh,'0'
  cmp bl,'3'
  je hasil3
  
  cmp bh,'0'
  cmp bl,'4'
  je hasil4
  
  cmp bh,'0'
  cmp bl,'5'
  je hasil5
  
  jne error_msg
  
;================================
hasil1:
  mov ah,09h
  lea dx,teks1
  int 21h
  int 20h
  
hasil2:
  mov ah,09h
  lea dx,teks2
  int 21h
  int 20h     
  
hasil3:
  mov ah,09h
  lea dx,teks3
  int 21h
  int 20h     
  
hasil4:
  mov ah,09h
  lea dx,teks4
  int 21h
  int 20h
  
hasil5:
  mov ah,09h
  lea dx,teks5
  int 21h
  int 20h
  
  
;================================='
teks1 db 13,10,'Anda memilih Citilink'
      db 13,10,'Dengan Nomor Penerbangan QG-702'
      db 13,10,'Denga Rute penerbangan Jakarta-Bali'
      db 13,10,'Pada 03 maret 2021'
      db 13,10,'Total Harga yang harus dibayar : Rp. 770.000'
      db 13,10,'Terima Kasih $'
      
teks2 db 13,10,'Anda memilih Sriwijaya Air'
      db 13,10,'Dengan Nomor Penerbangan SJ-226'
      db 13,10,'Denga Rute penerbangan Jakarta-Bali'
      db 13,10,'Pada 03 maret 2021'
      db 13,10,'Total Harga yang harus dibayar : Rp. 1.200.000'
      db 13,10,'Terima Kasih $'
      
teks3 db 13,10,'Anda memilih Lion Air'
      db 13,10,'Dengan Nomor Penerbangan JT-601'
      db 13,10,'Denga Rute penerbangan Jakarta-Bali'
      db 13,10,'Pada 03 maret 2021'
      db 13,10,'Total Harga yang harus dibayar : Rp. 550.000'
      db 13,10,'Terima Kasih $'                           
      
teks4 db 13,10,'Anda memilih Aviastar'
      db 13,10,'Dengan Nomor Penerbangan JMV-190'
      db 13,10,'Denga Rute penerbangan Jakarta-Bali'
      db 13,10,'Pada 03 maret 2021'
      db 13,10,'Total Harga yang harus dibayar : Rp. 720.000'
      db 13,10,'Terima Kasih $'                               
      
teks5 db 13,10,'Anda memilih Wings Air'
      db 13,10,'Dengan Nomor Penerbangan IW-220'
      db 13,10,'Denga Rute penerbangan Jakarta-Bali'
      db 13,10,'Pada 03 maret 20211'
      db 13,10,'Total Harga yang harus dibayar : Rp. 560.000'
      db 13,10,'Terima Kasih $'     
      
end start